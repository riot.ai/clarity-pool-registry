import { deployContract, faucetCall, mainnet, mocknet } from "../../src/deploy";
import { ADDR1, testnetKeyMap } from "../../src/mocknet";
import * as fs from "fs";
import { expect } from "chai";

export const deployer = mocknet
  ? testnetKeyMap[ADDR1]
  : JSON.parse(
      fs
        .readFileSync(
          "../../../github/blockstack/stacks-blockchain/keychain2.json"
        )
        .toString()
    );
const poolRegistryContract = mainnet ? "pool-registry-v1" : "pool-registry-v2";
describe("pool registry on testnet", async () => {
  it("fills the accounts", async () => {
    if (mocknet) {
      //await faucetCall(deployer.stacks, 80000000000000);
      await faucetCall("ST2PABAF9FTAJYNFZH93XENAJ8FVY99RRM4DF2YCW", 100000000);
    }
  });

  it("deploys", async () => {
    const result = await deployContract(
      `${poolRegistryContract}`,
      "./contracts/pool-registry.clar",
      (s) =>
        mainnet
          ? s.replace(
              /ST000000000000000000002AMW42H/g,
              "SP000000000000000000002Q6VF78"
            )
          : s,
      deployer.private
    );
    expect(result).to.be.a("string");
  });
});
