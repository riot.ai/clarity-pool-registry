import {
  bufferCV,
  contractPrincipalCV,
  cvToString,
  getAddressFromPrivateKey,
  listCV,
  makeContractCall,
  makeSTXTokenTransfer,
  noneCV,
  standardPrincipalCV,
  TransactionVersion,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { StackingClient } from "@stacks/stacking";
import { expect } from "chai";
import { readFileSync } from "fs";
import { describe } from "mocha";
import {
  deployContract,
  mainnet,
  mocknet,
  faucetCall,
  network,
  handleTransaction,
  timeout,
} from "../../src/deploy";
import { c32addressDecode } from "c32check";
import BN from "bn.js";
import {
  downloadPoxTxs,
  getDelegationStates,
  getFilteredDelegationStates,
  infoApi,
  poolToolContract,
  poxContractAddress,
  stackDelegatedStxsInBatches,
  writeDelegationStates,
} from "../../src/pool-tool-utils";
import { ADDR1, testnetKeyMap } from "../../src/mocknet";
import { poxAddrCV } from "../../src/utils-pox-addr";

describe("pool tool suite", () => {
  it("fills the accounts", async () => {
    if (mocknet) {
      await faucetCall(
        "ST2MY1BVKR8W8NF58N2GX6JDZDRT5CXP6RVZ097M4",
        80000000000000
      );
    }
  });

  it("deploys", async () => {
    const result = await deployContract(
      `${poolToolContract.name}`,
      "./contracts/pool-tool.clar",
      (s) =>
        mainnet
          ? s.replace(
              /ST000000000000000000002AMW42H/g,
              "SP000000000000000000002Q6VF78"
            )
          : s,
      poolAdmin.private
    );
    expect(result).to.be.a("string");
  });

  it("delegate", async () => {
    const stacking = new StackingClient(user.stacks, network);
    const result = await stacking.delegateStx({
      amountMicroStx: new BN(200000000000000),
      delegateTo: poolAdmin.stacks,
      privateKey: user.private,
    });
    console.log(result);
  });

  it("authorizes the pool-tool", async () => {
    const tx = await makeContractCall({
      contractAddress: poxContractAddress,
      contractName: "pox",
      functionName: "allow-contract-caller",
      functionArgs: [
        contractPrincipalCV(poolToolContract.address, poolToolContract.name),
        noneCV(),
      ],
      senderKey: poolAdmin.private,
      network: network,
    });
    await handleTransaction(tx);
  });

  it("save states", async () => {
    const txs = await downloadPoxTxs(network);
    await writeDelegationStates(txs, "");
  });

  it.only("replaces", async () => {
    const tx = await makeSTXTokenTransfer({
      amount: new BN(10),
      recipient: "SP287WSB9DMKVWND9JQKZ3H9TY9MSDV9QEQ8SJ3TP",
      senderKey: poolAdmin.private,
      network,
      fee: new BN(10000),
      nonce: new BN(10),
    });
    await handleTransaction(tx);
  });

  it("stacks delegated stx", async () => {
    const info = await infoApi.getCoreApiInfo();
    const startBurnHeight = 676300; //info.burn_block_height + 3; // + 1 or more, just in case the node is behind.
    const cycleId = mainnet ? 5 : 124;

    await stackDelegatedStxsInBatches(
      indices,
      length,
      rewardKeys,
      startBurnHeight,
      lockingPeriod,
      poolAdmin
    );
  });

  it("commits", async () => {
    const cycleId = mainnet ? 5 : 124;
    const tx = await makeContractCall({
      contractAddress: poxContractAddress,
      contractName: "pox",
      functionName: "stack-aggregation-commit",
      functionArgs: [poxAddrCV(rewardKeys.stacks), uintCV(cycleId)],
      senderKey: poolAdmin.private,
      network: network,
    });

    console.log(tx);
    await handleTransaction(tx);
  });

  it("get stacking info", async () => {
    const principal = "SP27ZT4JVW342YH03AMHFMBBAM89AAA9GV4DPAHX4"; //poolAdmin.stacks
    const stacking = new StackingClient(principal, network);
    const status = await stacking.getStatus();
    console.log(status);
  });
});
