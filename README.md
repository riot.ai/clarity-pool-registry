# Stacking Pool Registry

## Stacking and Stacking Pools

The [Stacks chain](https://stacks.co) uses Proof of Transfer (PoX) as the consensus algorithm. This involves users to stack (lock) their Stacks token during reward cycles. During a reward cycle, Stack miners send Bitcoins to the Stackers. The number of Bitcoin transactions is limited to 2000 per reward cycle. Therefore, are required to stack a minimum of Stacks tokens depending of the overall participation in stacking. Users that do not want to stack the minimum of Stacks tokens can join a stacking pool. The PoX contract defines the protocol that allows users to participate in stacking with smaller amounts as long as the total of pooled Stacks token is above the minimum.

## Stacking Pool Providers

A stacking pool registers one Bitcoin address and distributes the received rewards between its pool members. How and when this happens and in which currency the payout happens can be decided by the pool provider. The pool provider can be a trust worthy person, a DAO, an exchange, .. The pool members have to trust that the rewards are paid out in the promised way.

Pool providers also defines the Bitcoin reward address and they have to commit the pooled Stacks token in each cycle.

Read the full article: [How to run a Stacking Pool](https://app.sigle.io/friedger.id/UOvy85BCSD-bjlrv_6q74)

## Pool Registry

The pool registry store information about the pool providers on-chain such that they can be found globally. There is no special requirement to register a pool other than sending a `register` contract call to the registry contract. The `register` function takes

- a name (currently not verified)
- a Bitcoin address
- a url to more off-chain meta data
- an optional pox contract that users have to use to join the pool if no extended pox contract is defined.
- an optional extended pox contract that users have to use to join the pool if no pox contract is defined.
- an optional minimum amount of Stacks token to join the pool
- a payout currency

## PoX Contract

The PoX contract defines the function `delegate-stx` that users have to use to join a pool. Pool providers can decide to deploy their own contract with a `delegate-stx` function that then calls the original function. The corresponding trait for that function is defined in the registry contract.

Note, if a pool uses their own contracts, then users have to authorize that contract to manage their stacking activities in a separate transaction. For this, the origin PoX contract defines `allow-contract-caller` and `revoke-contract-caller`.

THe pool provider has the choice between two traits, one that is implemented by the genesis pox address, one that has two more arguments.

### PoX Contract Trait

The trait defines the exact signature that the pox contract needs to implement. It only contains one function `delegate-stx`. This function is documented in the official documents [here](https://docs.blockstack.org/references/stacking-contract#delegate-stx).

```
(define-trait pool-trait
    ((delegate-stx
        (uint
            principal
            (optional uint)
            (optional (tuple (hashbytes (buff 20)) (version (buff 1)))))
        (response bool int))))
```

The meaning of the parameters are:

- `amount-ustx` The maximum amount of stx (in micro stacks) that the delegatee can stack for the user. This can be higher than the current balance. The user's balance is only relevant when the delegatee locks the user's stacks. The delegatee cannot stack more than current balance even though the user delegated more than that earlier.
- `delegate-to` The delegatee
- `until-burn-ht` The bitcoin block height when locking has to end. Thereafter, the delegation is still valid but meaningless. The delegatee can't lock any user's token. The user does not delegate. The user has to revoke delegation and set a new delegation. If none is set there is no defined end of the delegation relation between user and delegatee. The user has to explicitly revoke delegation to end the relationship.
- `pox-addr` The bitcoin address that the delegatee must use to receive the Bitcoin rewards. This is useful mainly for transparency. The user can review the Bitcoin address and its transactions.

### PoX Contract Extended Trait

The trait defines the same function as the base version with two more arguments:

```
(define-trait pool-trait
    ((delegate-stx
        (uint
            principal
            (optional uint)
            (optional (tuple (hashbytes (buff 20)) (version (buff 1))))
            (optional (tuple (hashbytes (buff 20)) (version (buff 1))))
            uint)
        (response bool int))))
```

The extra two arguments are

- `user-pox-addr` The optional address that the delegatee must use to send the rewards to. This is the hash of the public key. It can be used to determine a btc address, a stx address or maybe other addresses. This is useful to communicate the reward address of the delegator.
- `locking-period` The number of cycles for which the delegated stacks should be locked. This is useful to communicate the desired length of locking period to the pool admin. If the number is not relevant for the pool the value 0 should be used here.

### Implementation Rules

Implementations are expected to call the `delegate-stx` functions of genesis contract `SP000000000000000000002Q6VF78.pox`. The contract may implement a `revoke-delegate-stx`. However, users can call the function on the genesis contract directly to revoke the delegation.

## Tools

See [tools.md](tools.md) for documentation of the tools contained in this repo.

