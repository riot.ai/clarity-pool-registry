import { makeContractCall, uintCV } from "@stacks/transactions";
import { handleTransaction, network } from "../src/deploy";
import { poxContractAddress } from "../src/pool-tool-utils";
import { poxAddrCV, poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BN from "bn.js";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq"
  //"1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF"
);

(async () => {
  const cycleId = 7;
  const admin = pool700;
  const tx = await makeContractCall({
    contractAddress: poxContractAddress,
    contractName: "pox",
    functionName: "stack-aggregation-commit",
    functionArgs: [rewardPoxAddrCV, uintCV(cycleId)],
    senderKey: admin.private,
    network: network,
  });

  console.log(tx);
  await handleTransaction(tx);
})();
