import { network } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
} from "../src/pool-tool-utils";

(async () => {
  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network
  );
  console.log(v1Stackers.length);
  const stackers = v0Stackers.concat(v1Stackers);
  //const cycle3 = stackersToCycle(stackers, 3);
  //const cycle4 = stackersToCycle(stackers, 4);
  //const cycle5 = stackersToCycle(stackers, 5);
  //const cycle6 = stackersToCycle(stackers, 6);
  const cycle7 = stackersToCycle(stackers, 7);

  console.log(JSON.stringify(cycle7));
})();
