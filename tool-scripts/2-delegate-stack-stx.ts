import { cvToString } from "@stacks/transactions";
import { readFileSync } from "fs";
import { mainnet, network } from "../src/deploy";
import {
  downloadPoxTxs,
  infoApi,
  logDelegationStatesCSV,
  stackDelegatedStxsInBatches,
  writeDelegationStates,
} from "../src/pool-tool-utils";
import { poxAddrCV, poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq"
  // retired "1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF"
);

(async () => {
  const length = 10;
  // Change here start
  const indices = undefined;
  const lockingPeriod = 1;
  const admin = pool6cycles;
  const minUntilBurnHt = 682850; // 699550 // 693250 // 686950 //682750
  const onlyExactUntilBurnHt = true;
  // change here end
  const info = await infoApi.getCoreApiInfo();
  console.log(info.burn_block_height);
  const startBurnHeight = 680500; //info.burn_block_height + 3; // + 1 or more, just in case the node is behind.
  const cycleId = mainnet ? 7 : 124;

  console.log(cvToString(rewardPoxAddrCV));

  await stackDelegatedStxsInBatches(
    indices,
    length,
    rewardPoxAddrCV,
    startBurnHeight,
    lockingPeriod,
    minUntilBurnHt,
    cycleId,
    onlyExactUntilBurnHt,
    admin
  );
})();
