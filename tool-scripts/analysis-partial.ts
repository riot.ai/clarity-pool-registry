import { getPartialStacked } from "../src/pool-tool-partials";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;
const btcAddr1 = "1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF";
const btcAddr2 = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";

(async () => {
  const cycleId = 7;

  console.log(await getPartialStacked(poolAdmin.stacks, cycleId, btcAddr1));
  console.log(await getPartialStacked(pool3cycles.stacks, cycleId, btcAddr1));
  console.log(await getPartialStacked(pool6cycles.stacks, cycleId, btcAddr1));
  console.log(await getPartialStacked(poolCcycles.stacks, cycleId, btcAddr1));

  console.log("===================================");
  let total = 0;
  for (let adminName of Object.keys(keys)) {
    const admin = (keys as any)[adminName];
    const partial = await getPartialStacked(admin.stacks, cycleId, btcAddr2);
    if (partial) {
      if (partial.amount === "none") {
        console.log(
          partial.cycle,
          partial.poolAdmin,
          partial.btcAddress,
          "none"
        );
      } else {
        const amount = parseInt(partial.amount);
        total += amount;
        console.log(
          partial.cycle,
          partial.poolAdmin,
          partial.btcAddress,
          (amount / 1000000).toLocaleString(undefined, {
            style: "currency",
            currency: "STX",
          })
        );
      }
    }
  }
  console.log("===================================");
  console.log(
    "total:",
    (total / 1000000).toLocaleString(undefined, {
      style: "currency",
      currency: "STX",
    })
  );
})();
